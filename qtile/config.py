#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os 
import subprocess
import re
import pickle
from libqtile import bar, hook, layout, widget
from libqtile.command import lazy
from libqtile.config import Click, Drag, Group, Key, Screen

STARTUP = ['nm-applet', 'vattery', 'start-pulseaudio-x11']

@hook.subscribe.startup_once
def autostart():
	path = os.path.dirname(os.path.abspath(__file__))
	subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr'])
	for program in STARTUP:
		subprocess.Popen([program, '&'])

def backlight_toggle(qtile):
	"""Toggle screen backlight, requires xbacklight"""
	screenbacklight = int(float(re.sub(r"\D", "", str((subprocess.Popen('xbacklight', stdout=subprocess.PIPE)).stdout.read()))) / 1000000)
	if screenbacklight > 0:
		subprocess.Popen(['xbacklight','-set','0'])
		pickle.dump(screenbacklight, open((os.path.dirname(os.path.abspath(__file__)) + "/.backlight.level"), "wb"))
	else:
		screenbacklight_old = pickle.load( open((os.path.dirname(os.path.abspath(__file__)) + "/.backlight.level"), "rb"))
		subprocess.Popen(['xbacklight','-set', str(screenbacklight_old)])
		subprocess.Popen(['notify-send', "Backlight: " + str(screenbacklight_old) + "%", '-t', '50'])

def backlight_inc(qtile):
	"""Increases backlight, requires xbacklight"""
	subprocess.Popen(['xbacklight','-inc','5'])
	subprocess.Popen(['notify-send', "Backlight: " + str(int(float(re.sub(r"\D", "", str((subprocess.Popen('xbacklight', stdout=subprocess.PIPE)).stdout.read()))) / 1000000)) + "%",  '-t', '50'])

def backlight_dec(qtile):
	"""Increases backlight, requires xbacklight"""
	subprocess.Popen(['xbacklight','-dec','5'])
	subprocess.Popen(['notify-send', "Backlight: " + str(int(float(re.sub(r"\D", "", str((subprocess.Popen('xbacklight', stdout=subprocess.PIPE)).stdout.read()))) / 1000000)) + "%", '-t', '50'])

LIGHT = False

if LIGHT:
	COLOR_BG = '#dedede'
	COLOR_HILIGHT = '#08b089'
	COLOR_HILIGHT_ALT = '#bdbdbd'
	COLOR_ACCENT = '#08b089'
	COLOR_FG = '#616161'
else:
	COLOR_BG = '#212121'
	COLOR_HILIGHT = '#f74f76'
	COLOR_HILIGHT_ALT = '#424242'
	COLOR_ACCENT = '#ff1744'
	COLOR_FG = '#9e9e9e'

mod = 'mod4'

keys = [

	# Window Manager
	Key([mod, 'control'], 'r', lazy.restart()),
	Key([mod, 'control'], 'q', lazy.shutdown()),
	Key([mod], 'Return', lazy.spawn('dmenu_run -q -h 24 -p ~ -fn "FiraMono-12" -nb ' + COLOR_BG + ' -nf ' + COLOR_FG + ' -sb ' + COLOR_HILIGHT_ALT + ' -sf ' + COLOR_HILIGHT)),# -show run -font 'Telex 12' -fg '" + COLOR_FG + "' -bg '" + COLOR_BG + "' -hlfg '" + COLOR_HILIGHT + "' -hlbg '" + COLOR_BG + "' -bc '" + COLOR_FG + "'")),
	Key([mod, 'control'], 'Return', lazy.spawncmd('fish')),
	Key([mod, 'mod1'], 'Return', lazy.spawn('st')),
	Key([mod], 'w', lazy.window.kill()),

	# Layout
	Key([mod], 'space', lazy.layout.next()),
	Key([mod, 'mod1'], 'space', lazy.next_layout()),
	Key(['mod1'], 'space', lazy.hide_show_bar()),

	# Move Windows
	Key([mod], 'Left', lazy.layout.down()),
	Key([mod], 'Right', lazy.layout.up()),
	Key([mod, 'control'], 'Right', lazy.layout.up()),
	Key([mod, 'control'], 'Right', lazy.layout.up()),

	# Axuillary Keys
	Key([mod], 'F8', lazy.function(backlight_dec)),
	Key([mod], 'F9', lazy.function(backlight_inc)),
	Key([mod], 'F2', lazy.function(backlight_toggle)),
	
	Key([mod], 'F7', lazy.spawn("setxkbmap 'us' 'norman' ")),
	Key([mod, 'control'], 'F7', lazy.spawn("setxkbmap 'us' ")),
]

bring_front_click = True
cursor_warp = False
follow_mouse_focus = True

mouse = [
		Drag([mod], "Button1", lazy.window.set_position_floating(),
				start=lazy.window.get_position()),
		Drag([mod], "Button3", lazy.window.set_size_floating(),
				start=lazy.window.get_size()),
		Click([mod], "Button2", lazy.window.bring_to_front())
]

groups = [
	Group('Δ'),
	Group('Φ'),
	Group('λ'),
]

# mod + letter of group = switch to group
keys.append(Key([mod], 'a', lazy.group['Δ'].toscreen()))
keys.append(Key([mod], 's', lazy.group['Φ'].toscreen()))
keys.append(Key([mod], 'd', lazy.group['λ'].toscreen()))

# mod + shift + letter of group = switch to & move focused window to group
keys.append(Key([mod, 'shift'], 'a', lazy.window.togroup('Δ')))
keys.append(Key([mod, 'shift'], 's', lazy.window.togroup('Φ')))
keys.append(Key([mod, 'shift'], 'd', lazy.window.togroup('λ')))

dgroups_key_binder = None
dgroups_app_rules = []

layouts = [
	layout.Max(),
	layout.Tile(
		shift_windows=True,
		add_on_top=False,
		border_width=2,
		border_focus=COLOR_ACCENT,
		border_normal=COLOR_FG,
		),
]

floating_layout = layout.Floating(
	border_width=2,
	border_focus="#0782B2",
	border_normal=COLOR_FG,
	auto_float_types=["notification","toolbar","splash","dialog",]
)

widget_defaults = dict(
	font='InputMono',
	fontsize=16,
	padding=0,
	foreground=COLOR_FG,
)

screens = [
	Screen(
		bottom=bar.Bar(
			widgets=[
				widget.GroupBox(
					active=COLOR_ACCENT,
					inactive=COLOR_FG,
					urgent_text=COLOR_HILIGHT,
					margin_y=3,
					margin_x=3,
					borderwidth=4,
					highlight_method="block",
					this_current_screen_border=COLOR_HILIGHT_ALT,
					padding=3.5,
					urgent_border=COLOR_HILIGHT,
					urgent_alert_method="text",
					disable_drag=True,
				),
				widget.Spacer(width=30),
				widget.Prompt(foreground=COLOR_ACCENT),
				widget.Spacer(width=30),
				widget.TaskList(highlight_method="block", padding=3.5, urgent_border=COLOR_HILIGHT, border=COLOR_HILIGHT_ALT, max_title_width=430),
				widget.Spacer(width=30),
				widget.Systray(icon_size=24),
				widget.Spacer(width=30),
				widget.ThermalSensor(foreground=COLOR_FG),
				widget.Clock(format=" | %a %b %d | %I:%M %P"),
				widget.Spacer(width=30),
			],
			size=32,
			background=COLOR_BG,
		),
	),
]

# Application specific tweaks

Float_Apps = ['Syncplay configuration', 'Steam Login', 'Minecraft Launcher', 'SUSE', 'ColorGrab', 'VNC Viewer: Connection Details']

@hook.subscribe.client_new
def float_spawner(window):

	window_name = window.window.get_name()
	for app in Float_Apps:
		if window_name.startswith(app):
			window.floating = True

	if (window.window.get_wm_type() == 'dialog' or window.window.get_wm_transient_for()):
		window.floating = True
