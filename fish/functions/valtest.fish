function valtest
	curl $argv > program.zip
	bsdtar -xf ./program.zip
	rm ./program.zip
	g++ ./program/*.c
	valgrind ./program/*.c
	rm -r ./program
end