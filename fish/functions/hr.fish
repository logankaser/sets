#!/usr/bin/fish

function hr
	if test $TERM = "linux"
		set char '-'
	else
		set char '—' 
	end
	for x in (seq $COLUMNS);
		echo -n $char;
	end
end
