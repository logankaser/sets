#!/usr/bin/fish
# Ask user a question then return anwser by echoing choice.

function question
	while true
		read -p 'set_color "white"; echo -n "(";set_color "normal";echo -n $argv;set_color "white";echo -n "): ";set_color "normal"' -l choice
		# Pretty input error handling
    echo $choice
    break
	end
end
