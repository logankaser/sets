#!/usr/bin/fish
# Displays a menu then returns the user's choice be echoing.

function menu
	while true
		read -p 'set_color white; echo -n "(";set_color normal;echo -n $argv;set_color white;echo -n "): ";set_color normal' -l choice
		# Pretty input error handling
    echo $choice
    break
	end
end
