#!/usr/bin/fish
# Ask user a yes or no question, then return 0 for yes and 1 for no as a $status.

function confirm
	while true
		read -p 'set_color white; echo -n "(";set_color green;echo -n "yes";set_color white;echo -n "/";set_color red;echo -n "no";set_color white;echo -n "): ";set_color normal' -l choice
		switch $choice
			case Y y yes Yes ye yup
				return 0
				break
			case '' N n no No nah nope
				return 1
				break
		end
	end
end
