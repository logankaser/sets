#!/usr/bin/fish

function lf
	clear;set_color green;echo (pwd);set_color normal; ls --file-type --group-directories-first;hr
end
