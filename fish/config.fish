# Fish config file ~/.config/fish/config.fish

# Remove greeting.
set -gx fish_greeting ''

# Make man more usable
set -gx MANPAGER less

# Set path.
set -gx PATH $PATH ~/.cabal/bin
set -gx PATH $PATH ~/.local/bin
set -gx PATH $PATH ~/.bin
set -gx PATH $PATH /opt/void-packages

# Linux
alias s sudo

# XBPS
alias xi "sudo xbps-install"
alias xq "sudo xbps-query"
alias xr "sudo xbps-remove"
alias xs "sudo xbps-src"

# Wifish aliases
alias wi "wpa_cli scan;sleep 1;wifish"
alias wl "wpa_cli list_networks"
alias ws "wpa_cli status"
alias wr "sudo wpa_cli remove_network;wpa_cli save_config"
alias wsn "wpa_cli select_network"

# Utilities
alias ac "acpi" # get your power status.
alias dv "dvtm -m \^s" # start dvtm; set  ctrl-s  as the mod key.
